create table runs(
system text,
id int,
name text,
n int,
tps real,
project text,
new int,
continued int,
overwrote int,
parent int,
child int,
dir text,
hardware text,
submitted text,
tstart text,
tstop text,
ttotal text,
pbsid text,
crashed int,
trusted text,
comment text);
--CREATE TABLE runs(id int, name text, submitted text, tstart text, tstop text, ttotal text, comment text, dir text, tps real, hardware text, crashed int, new int, continued int, pbsid text, system text, trusted int, project text, overwrote int, parent int, child int, n int, primary key (id,system));
attach '1.db' as db1;
attach '2.db' as db2;
attach '3.db' as db3;
attach '5.db' as db5;
insert into runs select system,id,name,n,tps,project,new,continued,overwrote,parent,child,dir,hardware,submitted,tstart,tstop,ttotal,pbsid,crashed,trusted,comment from db1.runs;
insert into runs select system,id,name,n,tps,project,new,continued,overwrote,parent,child,dir,hardware,submitted,tstart,tstop,ttotal,pbsid,crashed,trusted,comment from db2.runs;
insert into runs select system,id,name,n,tps,project,new,continued,overwrote,parent,child,dir,hardware,submitted,tstart,tstop,ttotal,pbsid,crashed,trusted,comment from db3.runs;
insert into runs select system,id,name,n,tps,project,new,continued,overwrote,parent,child,dir,hardware,submitted,tstart,tstop,ttotal,pbsid,crashed,trusted,comment from db5.runs;

