import numpy as np
import csv
import glob

#glob lets us use unix like wild cards

list_of_my_pfiles = glob.glob("m*/*.py")

toCSV = [] #list of dics to make into a csv

for py_file in list_of_my_pfiles:

    raw_py_file = np.loadtxt(py_file, dtype='str', delimiter="\n")
    # parses files
    y = raw_py_file[-2][12:-1]
    z = [raw_py_file.strip() for raw_py_file in y.split(',')]

    my_dict = {}

    my_dict = {i.replace("'","").replace(" ", "").split("=")[0] : i.replace("'","").replace(" ", "").split("=")[1] for i in z}

    o_file = py_file[:-2] + "o"

    a = np.loadtxt(o_file, dtype='str', delimiter="\n")
    # this fixes empty files
    if len(a) < 2:
        my_dict["run_success"] = "no o_file"
    else:
        b = (a[-2])
        my_dict["run_success"] = b
    toCSV.append(my_dict)

# check to find the biggist dictionary

dic_with_largest_key = -1
max_key_len = -1

for i, dic in enumerate(toCSV):
    cur_key_len = len(dic.keys())
    if cur_key_len >= max_key_len:
        max_key_len = cur_key_len
        dic_with_largest_key = i


keys = toCSV[dic_with_largest_key].keys()
with open('people.csv', 'wb') as output_file:
    dict_writer = csv.DictWriter(output_file, keys)
    dict_writer.writeheader()
    dict_writer.writerows(toCSV)
