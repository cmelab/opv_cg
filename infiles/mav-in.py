"""Main input file for running a hoomd-blue opv_cg simulation."""
from cme_utils.job_control import manager
from random import randint
import os
import sys
home_dir = os.environ['HOME']
sim_db_filename = home_dir+'/sim_db.sqlite3'
run_dir = 'runs/'
#################################################
email = "ericjankowski@boisestate.edu"
allocation = 'TG-DMR140097' #tacc full research
queue_name = 'gpu'  #maverick
project = "mike4-bdt"
desired_hours = 3.0
submit_job = True
run_type = 'new'

model_dir = "mlibs/models/nrel_aa/"
model_file = model_dir+"model.xml"
model_name = "ua_e"

N = 197
bb_file = {}
bb_num = {}
bb_file['bdt']=model_dir+'bdt_d_ua_remix.xml'
bb_num['bdt'] = N

parent =0 # 0,None start new jobs, 'last' depends on the last job, and a number>0 will depend on that job
if run_type=='new':
    parent = 0
parent_file = "init.xml"
T = 4.0
phi= 1.1
e_factor = 0.5

dt= 0.0001
tauP= 5.
tauT= 5.
tmax = 2e9
log_write_time = 1e5
dcd_write_time = 1e6
mix_time = 5e5
mixT = 3.5
shrink_time = 5e5
shrinkT = 3.5
P = 0.5
ensemble="NVT"
seed = randint(0,2**32) 
estimate_tps = 200.

custom_prefix = '{}-dt{}-phi{}-e{}-P{}'.format(project,dt,phi,e_factor,P)  
name_pattern="{0}-T{1}"
run_name = name_pattern.format(custom_prefix,T)

######################
members = [ i for i in dir() if (not i.startswith('__')) and (not callable(i)) and (i not in ['opv','manager','sys','os'] ) ]
params ={}
for m in members:
    params[m] = locals()[m]
manager.HoomdRun(**params) #magic happens in here: run is added, paths are set, etc.
