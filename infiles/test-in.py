"""Main input file for running a hoomd-blue opv_cg simulation."""
from cme_utils.job_control import manager
from random import randint
import os
import sys
home_dir = os.environ['HOME']
user = os.environ['USER']
run_dir = "runs/"
#run_dir = "/scratch/{}/opv_cg/runs/".format(user)
sim_db_filename = 'sim_db.sqlite3'
#################################################

email = "ericjankowski@boisestate.edu"
queue_name = 'none' #kestrel
project = "perylothiophene-test"
desired_hours =12.
submit_job = True
run_type = 'start_from'

model_dir = "mlibs/models/perylothiophene/"
model_file = model_dir+"model.xml"
model_name = "default"

bb_file = {}
bb_num = {}
molecule = 'perylothiophene'
n_mol=1
bb_file[molecule]=model_dir+'perylothiophene.xml' #No rigid models at the moment
bb_num[molecule] = n_mol

parent = 157# 0,None start new jobs, 'last' depends on the last job, and a number>0 will depend on that job
#parent = 39 # 0,None start new jobs, 'last' depends on the last job, and a number>0 will depend on that job
#parent = 'last' # 0,None start new jobs, 'last' depends on the last job, and a number>0 will depend on that job
if run_type=='new':
    parent = 0
parent_file = "init.xml"
e_factor = 1.0 #float(sys.argv[1])
P = 1.0 #float(sys.argv[2])
T = 1.0
mixT = 13.0
dt= 0.0003
tauP= 5.
tauT= 5.
phi= 0.001
tmax = 2e6
log_quants = ['ndof']
log_write_time = 1e5
dcd_write_time = 1e5
mix_time = 1e5
shrink_time = 1e5
shrinkT = 13.0
ensemble="NVT"
seed = randint(0,2**32) 
estimate_tps = 2000.

custom_prefix = '{}-dt{}-phi{}-e{}-P{}'.format(project,dt,phi,e_factor,P)  
name_pattern="{0}-T{1}"
run_name = name_pattern.format(custom_prefix,T)

######################
members = [ i for i in dir() if (not i.startswith('__')) and (not callable(i)) and (i not in ['opv','manager','sys','os'] ) ]
params ={}
for m in members:
    params[m] = locals()[m]
manager.HoomdRun(**params) #magic happens in here: run is added, paths are set, etc.
