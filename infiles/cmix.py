"""Main input file for running a hoomd-blue opv_cg simulation."""
from cme_utils.job_control import manager
from random import randint
import os
import sys
home_dir = os.environ['HOME']
sim_db_filename = 'sim_db.sqlite3'
#sim_db_filename = home_dir+'/sim_db.sqlite3'
run_dir = 'runs/'
#################################################
email = "vmg5091@psu.edu"
queue_name='none'
project = "c60tests"
desired_hours = 1.0
submit_job = True
run_type = 'new'

model_dir = "mlibs/models/C60/"
model_file = model_dir+"model.xml"
model_name = "default"

bb_file = {}
bb_num = {}
bb_file['neutral']=model_dir+'c60-c.xml'
bb_file['charged']=model_dir+'c60-ab.xml'
bb_num['neutral'] = 150
bb_num['charged'] = 150

parent =0 # 0,None start new jobs, 'last' depends on the last job, and a number>0 will depend on that job
if run_type=='new':
    parent = 0
parent_file = "restart.xml"
T = 10.0
P = 1
dt= 0.001
tauT= 5.0
phi= 0.8
tmax = 2e9
log_write_time = 1e4
dcd_write_time = 1e4
randomize_flag = True
mix_time = 1e5
shrink_time = 1e5
shrinkT = 10.0
ensemble="NVT"
seed = randint(0,2**32) 
estimate_tps = 7000

run_name = "{}-{}".format(project,T)

######################
members = [ i for i in dir() if (not i.startswith('__')) and (not callable(i)) and (i not in ['opv','manager','sys','os'] ) ]
params ={}
for m in members:
    params[m] = locals()[m]
manager.HoomdRun(**params) #magic happens in here: run is added, paths are set, etc.
