"""Main input file for running a hoomd-blue opv_cg simulation."""
from cme_utils.job_control import manager
from random import randint
import os
import sys
home_dir = os.environ['HOME']
user = os.environ['USER']
run_dir = "/scratch/{}/opv_cg/runs/".format(user)
sim_db_filename = 'sim_db.sqlite3'
#################################################

email = "mitchellleibowitz@u.boisestate.edu"
username = 'mitchellleibowitz'
queue_name = 'batch' #kestrel
project = "m-bdt"
desired_hours =12.
submit_job = True
run_type = 'new'

model_dir = "mlibs/models/nrel_aa/"
model_file = model_dir+"model.xml"
model_name = "ua_e"

bb_file = {}
bb_num = {}
molecule = 'bdt'
n_mol=197
bb_file[molecule]=model_dir+'bdt_d_ua_remix.xml' #No rigid models at the moment
bb_num[molecule] = n_mol

parent = 0# 0,None start new jobs, 'last' depends on the last job, and a number>0 will depend on that job
#parent = 39 # 0,None start new jobs, 'last' depends on the last job, and a number>0 will depend on that job
#parent = 'last' # 0,None start new jobs, 'last' depends on the last job, and a number>0 will depend on that job
if run_type=='new':
    parent = 0
parent_file = "restart.xml"
e_factor = 0.8 #float(sys.argv[1])
P = 1.0 #float(sys.argv[2])
T = 4.0
mixT = 4.0
dt= 0.0005
tauP= 5.
tauT= 5.
phi= 1.0
tmax = 2e9
log_write_time = 1e5
dcd_write_time = 1e6
mix_time = 1e6
shrink_time = 1e6
shrinkT = 4.5
ensemble="NVT"
seed = randint(0,2**32) 
estimate_tps = 200.

custom_prefix = '{}-dt{}-phi{}-e{}-P{}'.format(project,dt,phi,e_factor,P)  
name_pattern="{0}-T{1}"
run_name = name_pattern.format(custom_prefix,T)

######################
members = [ i for i in dir() if (not i.startswith('__')) and (not callable(i)) and (i not in ['opv','manager','sys','os'] ) ]
params ={}
for m in members:
    params[m] = locals()[m]
manager.HoomdRun(**params) #magic happens in here: run is added, paths are set, etc.
