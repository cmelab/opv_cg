"""Main input file for running a hoomd-blue opv_cg simulation."""
from cme_utils.job_control import manager
from random import randint
import os
import sys
home_dir = os.environ['HOME']
sim_db_filename = 'sim_db.sqlite3'
#sim_db_filename = home_dir+'/sim_db.sqlite3'
run_dir = 'runs/'
#################################################
email = "ericjankowski@boisestate.edu"
queue_name='debug'
project = "c60_debug"
desired_hours = 0.5
submit_job = True
run_type = 'new'

model_dir = "mlibs/models/C60/"
model_file = model_dir+"model.xml"
model_name = "default"

bb_file = {}
bb_num = {}
bb_file['c60O']=model_dir+'c60-ab.xml'
bb_num['c60O'] = 30

parent =0 # 0,None start new jobs, 'last' depends on the last job, and a number>0 will depend on that job
if run_type=='new':
    parent = 0
parent_file = "restart.xml"
T = 3.0
P = 1
e_factor = 1.0
dt= 0.0005
tauP= 5.
tauT= 5.0
phi= 0.1
tmax = 2e9
log_write_time = 1e4
dcd_write_time = 1e4
randomize_flag = False
mix_time = 1e3
shrink_time = 1e3
shrinkT = 3.0
ensemble="NVT"
pppm_gridsize=32
pppm_gridOrder = 6
pppm_rcut = 2.0
seed = randint(0,2**32) 
estimate_tps = 10000

custom_prefix = '{}-T{}'.format(project,T)
name_pattern="{0}-T{1}"
run_name = name_pattern.format(custom_prefix,T)

######################
members = [ i for i in dir() if (not i.startswith('__')) and (not callable(i)) and (i not in ['opv','manager','sys','os'] ) ]
params ={}
for m in members:
    params[m] = locals()[m]
manager.HoomdRun(**params) #magic happens in here: run is added, paths are set, etc.
