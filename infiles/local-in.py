"""Main input file for running a hoomd-blue opv_cg simulation."""
from cme_utils.job_control import manager
from random import randint
import os
import sys
home_dir = os.environ['HOME']
sim_db_filename = 'sim_db.sqlite3'
#sim_db_filename = home_dir+'/sim_db.sqlite3'
run_dir = 'runs/'
#################################################
email = "mitchellleibowitz@u.boisestate.edu"
queue_name='none'
<<<<<<< HEAD
project = "flex_perylene"
desired_hours = 4
=======
project = "c60_debug"
desired_hours = 1.0
>>>>>>> master
submit_job = True
run_type = 'new'

model_dir = "mlibs/models/C60/"
model_file = model_dir+"model.xml"
model_name = "default"

bb_file = {}
bb_num = {}
<<<<<<< HEAD
bb_file['charged_perylene']=model_dir+'stable_out1.xml'
bb_num['charged_perylene'] = N
=======
bb_file['c60O']=model_dir+'c60-ab.xml'
bb_num['c60O'] = 30
>>>>>>> master

parent =0 # 0,None start new jobs, 'last' depends on the last job, and a number>0 will depend on that job
if run_type=='new':
    parent = 0
parent_file = "restart.xml"
T = 1
P = 1
e_factor = 1.0
<<<<<<< HEAD
dt= 0.00005
tauP= 5.
tauT= 0.5
phi= 0.397
=======
dt= 0.0005
tauP= 5.
tauT= 5.0
phi= 0.1
>>>>>>> master
tmax = 2e9
log_write_time = 1e3
dcd_write_time = 1e5
randomize_flag = False
<<<<<<< HEAD
mix_time = 1e6
shrink_time = 1e6
shrinkT = 5
=======
mix_time = 1e3
shrink_time = 1e3
shrinkT = 3.0
>>>>>>> master
ensemble="NVT"
pppm_gridsize=32
pppm_gridOrder = 6
pppm_rcut = 2.0
seed = randint(0,2**32) 
<<<<<<< HEAD
estimate_tps = 370

custom_prefix = 'flex10CH-n_mol_C0.292_H0.486_phi_0.397'.format(project,N)
=======
estimate_tps = 1000

custom_prefix = '{}-T{}'.format(project,T)
>>>>>>> master
name_pattern="{0}-T{1}"
run_name = name_pattern.format(custom_prefix,T)

######################
members = [ i for i in dir() if (not i.startswith('__')) and (not callable(i)) and (i not in ['opv','manager','sys','os'] ) ]
params ={}
for m in members:
    params[m] = locals()[m]
manager.HoomdRun(**params) #magic happens in here: run is added, paths are set, etc.
