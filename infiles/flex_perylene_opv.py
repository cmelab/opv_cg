"""Main input file for running a hoomd-blue opv_cg simulation."""
from cme_utils.job_control import manager
from random import randint
import os
import sys
home_dir = os.environ['HOME']
user = os.environ['USER']
run_dir = "/scratch/{}/opv_cg/runs/".format(user)
sim_db_filename = 'sim_db.sqlite3'
#################################################

#email = "vgreenholt@yahoo.com"
email = "mitchellleibowitz@u.boisestate.edu"
#email = "evanmiller326@boisestate.edu"
queue_name = 'batch' #kestrel
project = "checkerboard_charged"
desired_hours = 48
submit_job = True
run_type = 'from_mav'
infile = "checkerboard_charged"

"""
if sys.argv[2]=='s':
    run_type = 'start_from'
if sys.argv[2]=='c':
    run_type = 'continue'
if sys.argv[2]=='n':
    run_type = 'new'
"""
model_dir = "mlibs/models/charged_perylene/"
model_file = model_dir+"model.xml"
model_name = "default"

bb_file = {}
bb_num = {}
molecule = 'flex_perylene'
n_mol= 200
bb_file[molecule]=model_dir+'stable_out1.xml' #No rigid models at the moment
#bb_file['perylene']=model_file
bb_num[molecule] = n_mol

parent = "from_mav" #None start new jobs, 'last' depends on the last job, and a number>0 will depend on that job
#parent = 39 # 0,None start new jobs, 'last' depends on the last job, and a number>0 will depend on that job
#parent = 'last' # 0,None start new jobs, 'last' depends on the last job, and a number>0 will depend on that job
if run_type=='new':
    parent = 0
parent_file = "restart.xml"
#e_factor = float(sys.argv[2])
e_factor = 1.0 #float(sys.argv[1])
P=1
#P =float(sys.argv[1])
T =float(sys.argv[1])
#T = 14.2
mixT = 18.0
dt= 0.0001
tauP= 5
tauT= 0.5
#tauT= 5.
#phi=float(sys.argv[1])
#phi=1
phi= 1.0
tmax = 2e9
log_write_time = 1e5
dcd_write_time = 1e6
mix_time = 1e6
shrink_time = 1e6
shrinkT = 5.
ensemble="NVT"
seed = randint(0,2**32) 
estimate_tps = 150
#custom_prefix = 'checkerboard{}C_0.292_H0.486__phi3.0717opv'.format(n_mol) 
custom_prefix = 'checkerboard_charged-n_mol{}_phi1.0opv'.format(n_mol)  
name_pattern="{0}-T{1}"
run_name = name_pattern.format(custom_prefix,T)

######################
members = [ i for i in dir() if (not i.startswith('__')) and (not callable(i)) and (i not in ['opv','manager','sys','os'] ) ]
params ={}
for m in members:
    params[m] = locals()[m]
manager.HoomdRun(**params) #magic happens in here: run is added, paths are set, etc.
