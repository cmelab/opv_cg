from hoomd_script import *
from cme_utils.manip import builder
from cme_utils.manip import ff 
from cme_utils.manip import hoomd_xml
from cme_utils.manip.initialize_system import interactions_from_xml_verbatim, interactions_from_xml_tabulated
from random import random, randint
from subprocess import check_output
import math
import numpy

def sphere_volume(radius=0.5):
    return 4.0 * math.pi * math.pow(radius, 3.0) / 3.0

def iround(x): #rounds a float to the nearest integer
    y = round(x) - 0.5
    return int(y) + (y>0)

def distance(a,b,L=50000.0): #calculates the distance between a and b, with periodic cell L
    dx = b[0] - a[0]
    dx = dx - L*iround(dx/L)
    dy = b[1] - a[1]
    dy = dy - L*iround(dy/L)
    dz = b[2] - a[2]
    dz = dz - L*iround(dz/L)
    r = math.sqrt(dx**2 + dy**2 + dz**2)
    return r

def pbc(vec,L=50000.0):
    for i in range(len(vec)):
        d = vec[i] - L/2.0 
        if d > 0.0:
            vec[i] = -L/2.0 + d
        d = -L/2.0 - vec[i]
        if d > 0.0:
            vec[i] = L/2.0 - d
    return vec

def randInCube(L):
    x = L*random() - L/2.0
    y = L*random() - L/2.0
    z = L*random() - L/2.0
    return numpy.array([x,y,z])

def randVec():
    rsq = 2.0
    while rsq>=1.0:
        r1 = 1.0 - 2.0* random()
        r2 = 1.0 - 2.0* random()
        rsq = r1*r1 +r2*r2
    rh = math.sqrt(1.0-rsq)
    return numpy.array([2.0*rh*r1, 2.0*rh*r2, 1.0-2.0*rsq])

def overlap(a,b,L):
    for pa in a.particles:
        for pb in b.particles:
            if distance(pa.position,pb.position,L) < (pa.diameter +pb.diameter)/2.0:
                return True
    return False

def molOK(trial, molecules, L):
    for m in molecules:
        if distance(trial.position, m.position,L)< trial.Radius + m.Radius:
            if overlap(trial,m, L):
                return False
    return True

def randomizeMolecules(molecules, factor=2.5):
    v_total = 0.0
    for m in molecules:
        m.calcRadius()
        v_total = v_total+sphere_volume(m.Radius)
    v_total = v_total * factor
    L = math.pow(v_total, 1.0/3.0)
    print("\n", L, "\n")
    setMolecules = []
    for m in molecules:
        print( "setting", molecules.index(m), "/", len(molecules))
        trial = randInCube(L -2.0*m.Radius)
        m.translate(trial, L)
        while not molOK(m, setMolecules, L):
            trial = randInCube(L-2.0*m.Radius)
            m.translate(trial, L)
        setMolecules.append(m)
    return data.boxdim(L=L)

def total_volume(molecules):
    vol = 0.
    for m in molecules:
        vol = vol + m.volume
    return vol

class molecule:
    def setIDs(self, first, last):
        self.firstID = int(first)
        self.lastID = int(last)
        self.position =  numpy.array([0,0,0])
    def setParticles( self,particles ):
        slice = []
        for i in range(self.firstID, self.lastID):
            slice.append(particles[i])
        self.particles = slice
        if len(self.particles)!=self.nParticles:
            exit(1)
    def center(self):
        self.calcCOM()
        self.position = self.com
        self.translate(numpy.array([0.,0.,0.]))
    def calcCOM(self):
        x,y,z = 0.,0.,0.
        for p in self.particles:
            x = x + p.position[0]
            y = y + p.position[1]
            z = z + p.position[2]
        n = float(len(self.particles))
        self.com = numpy.array([x/n, y/n, z/n])
        #self.translate(self.com)
    def calcRadius(self):
        #self.calcCOM()
        self.center()
        maxD = 0.0
        farP = 0
        for p in self.particles:
            d =distance(p.position,self.position)
            if d > maxD:
                maxD = d
                farP = self.particles.index(p)
        self.Radius = maxD + self.particles[farP].diameter/2.0
    def translate(self,vec, L = 50000.0):
        diff =  vec - self.position
        self.position = self.position + diff
        for p in self.particles:
            p.position = pbc(p.position + diff, L)

class acceptor(molecule):
    def __init__( self, diameter=3.0, body = 1, filename = '.np-12'):
        self.diameter = diameter
        self.body = body
        self.anchors = numpy.loadtxt(filename)
        self.nParticles = len(self.anchors)
        self.volume = sphere_volume(diameter/2.)
    def defineAndConnect( self, system ):
        #types
        for i in range(self.nParticles-1):
            self.particles[i].type = 'D'
            self.particles[i].body = self.body
        self.particles[-1].diameter = 1.0
        self.particles[-1].type = 'D'
        self.particles[-1].body = self.body
        #positions
        for a,p in zip(self.anchors, self.particles):
            p.position = (self.diameter)*a/9.0
        return system

class particle_from_xml(molecule):
    body_count = -1
    def __init__ (self, filename, index_offset):
        self.firstID = index_offset
        self.type = []
        self.diameter = []
        self.mass = []
        self.charge = []
        self.body= []
        self.bond = []
        self.angle = []
        self.dihedral = []

        xml = hoomd_xml.hoomd_xml()
        xml.read(filename)

        self.nParticles = int(xml.config.find('position').attrib['num'])
        self.xyz = numpy.zeros(shape=(self.nParticles,3))
        xyz = xml.config.find('position').text.split()
        for i in range(self.nParticles):
            pos = [float(x) for x in xyz[i*3:i*3+3]]
            self.xyz[i] = numpy.array(pos)

        last_body = -1
        for i,m in enumerate(xml.config.find('body').text.split()):
            m=int(m)
            if m ==-1:
                self.body.append( -1)
            else:
                if m>last_body:
                    last_body = m
                    particle_from_xml.body_count +=1
                self.body.append(particle_from_xml.body_count)
        for i in xml.config.find('type').text.split():
            self.type.append(i)
        for i in xml.config.find('charge').text.split():
            self.charge.append(float(i))
        for i in xml.config.find('mass').text.split():
            self.mass.append(float(i))
        for i in xml.config.find('diameter').text.split():
            self.diameter.append(float(i))
        b = xml.config.find('bond').text.split()
        n = int(xml.config.find('bond').attrib['num'])
        for i in range(0,n*3,3):
            self.bond.append( (b[i], self.firstID+int(b[i+1]), self.firstID+int(b[i+2]) ))
        b = xml.config.find('angle').text.split()
        n = int(xml.config.find('angle').attrib['num'])
        for i in range(0,n*4,4):
            self.angle.append( (b[i], self.firstID+int(b[i+1]), self.firstID+int(b[i+2]), self.firstID+int(b[i+3])) )
        b = xml.config.find('dihedral').text.split()
        n = int(xml.config.find('dihedral').attrib['num'])
        for i in range(0,n*5,5):
            self.dihedral.append(( b[i], self.firstID+int(b[i+1]), self.firstID+int(b[i+2]), self.firstID+int(b[i+3]), self.firstID+int(b[i+4])))

    def defineAndConnect(self, system):
        for i,a in enumerate(self.xyz):
            self.particles[i].position = a
        for i,a in enumerate(self.mass):
            self.particles[i].mass = a
        for i,a in enumerate(self.diameter):
            self.particles[i].diameter = a
        for i,a in enumerate(self.body):
            self.particles[i].body = a
        for i,a in enumerate(self.charge):
            self.particles[i].charge = a
        for i,a in enumerate(self.type):
            self.particles[i].type = a
        for a in self.bond:
            system.bonds.add(a[0],a[1],a[2])
        for a in self.angle:
            system.angles.add(a[0],a[1],a[2],a[3])
        for a in self.dihedral:
            system.dihedrals.add(a[0],a[1],a[2],a[3],a[4])
        self.volume = 0.
        for p in self.particles:
            self.volume = self.volume + sphere_volume(p.diameter/2.)
        return system

class opv_run():
    """Convenience class for defining a hoomd-blue opv_cg job."""
    def __init__(self, **kwargs ):
        self.e_factor = 1.
        self.bb_num = {}
        self.run_type = "new"
        self.T = 1.
        self.P = 1.
        self.phi = 0.2
        self.tauP = 5.
        self.tauT = 5.
        self.tmax = 1e6
        self.dt = 0.001
        self.limit = 0.01
        self.log_write_time = 1e3
        self.dcd_write_time = 1e4
        self.mix_time = 1e5
        self.mixT = 4.0
        self.shrink_time = 1e5
        self.shrinkT = 5.0
        self.ensemble = "NPT"
        self.estimate_tps = 200.
        self.seed = randint(0,2**32)
        self.initialize_new = False
        self.randomize_flag = False
        self.shrink_flag = False
        self.run_dir = "runs/"
        self.model_file=""
        self.model_name=""
        self.xml_args = {'vis':True, 'image' : True, 'velocity' : True, 'time_step':0}
        self.log_quants = ['potential_energy', 'kinetic_energy', 'pressure','temperature', 'volume','pair_lj_energy','pppm_energy']
        self.init_factor = 4.0
        for k,v in kwargs.items():
            self.__dict__[k] = v
        self.prefix = str(self.run_dir +self.run_name+"/")
        self.restart = str(self.prefix+"restart.xml")
        self.traj = str(self.prefix+"traj.dcd")
        self.log = str(self.prefix+"log")
        self.msd = str(self.prefix+"msd-log")
        self.infile = self.run_dir+self.parent_name+"/"
        if self.run_type == 'new':
            self.infile+="init.xml"
        if self.run_type in ['continue','start_from','from_mav' ]:#Evan added 'from_mav'
            self.infile+=self.parent_file
        self.infile = str(self.infile)

    def __repr__(self):
        members = [ i for i in dir(self) if (not i.startswith('__')) and not callable(eval("self.{}".format(i))) ]
        s = "opv_run("
        for m in members:
            s += '{} = {!r}, '.format(m,self.__dict__[m])
        s = s[:-2]
        s += ")"
        return s


    def get_params(self):
        params ={}
        members = [ i for i in dir(self) if (not i.startswith('__')) and not callable(eval("self.{}".format(i))) ]
        for m in members:
            params[m] = self.__dict__[m]
        return params
        
    def initialize(self,L=200.,factor=4.,outfile="pre-rand.xml"):
        molecules = []
        particle_types = []
        bond_types = []
        angle_types = []
        dihedral_types = []
        firstID = 0
        lastID = 0
        for k in self.bb_num.keys():
            for i in range(self.bb_num[k]):
                mol = particle_from_xml(self.bb_file[k], lastID)
                firstID = lastID
                lastID = lastID + mol.nParticles
                mol.setIDs(firstID,lastID)
                molecules.append( mol )
                particle_types = particle_types + mol.type
                bond_types = bond_types + [ b[0] for b in mol.bond ]
                angle_types = angle_types + [ b[0] for b in mol.angle]
                dihedral_types = dihedral_types + [ b[0] for b in mol.dihedral]
                #TODO Improper types
        particle_types = list(set(particle_types))
        bond_types = list(set(bond_types))
        angle_types = list(set(angle_types))
        dihedral_types = list(set(dihedral_types))
        system = init.create_empty(N=lastID, box=data.boxdim(L=L), particle_types = particle_types, bond_types=bond_types, angle_types=angle_types, dihedral_types=dihedral_types)

        for m in molecules:
            m.setParticles(system.particles)
            system = m.defineAndConnect(system)
        system.box = randomizeMolecules(molecules=molecules, factor=factor)
        dump.xml(filename=self.prefix+outfile, **self.xml_args)          
        del system, m, mol, molecules
        init.reset()

    def randomize(self,infile="pre-rand.xml",outfile="pre-shrink.xml"):
        #always just NVT randomize to begin with
        system = init.read_xml(filename=self.prefix+infile)
        lj, bonds, angles, dihedrals = interactions_from_xml_verbatim(system,factor=self.e_factor, model_file=self.model_file, model_name= self.model_name)
        integrate.mode_standard(dt=self.dt)
        rigid = group.rigid()
        nonrigid = group.nonrigid()
        if len(rigid) > 0:
            int_rig = integrate.nvt_rigid(group=rigid,T=self.mixT,tau=self.tauT)
        if len(nonrigid) > 0:
            int_non = integrate.nvt(group=nonrigid,T=self.mixT,tau=self.tauT)
        pppm = charge.pppm(group=group.charged())
        pppm.set_params(Nx=64,Ny=64,Nz=64,order=6,rcut=4.0)
        nlist.reset_exclusions(exclusions=['bond','angle','dihedral','body'])
        #dump.dcd(filename=self.prefix+'randomize.dcd', period=1e4, overwrite=True)
        run(self.mix_time)
        dump.xml(filename=self.prefix+outfile, **self.xml_args) 
        if len(rigid)>0:
            del int_rig
        if len(nonrigid)>0:
            del int_non
        del system, lj, bonds, angles, dihedrals, rigid, nonrigid, pppm
        init.reset()

    def shrink(self,infile="pre-shrink.xml",outfile="init.xml"):
        #always shrink with BD_NVT
        if self.ensemble == 'NPT':
            return
        system = init.read_xml(filename=self.prefix+infile)
        lj, bonds, angles, dihedrals = interactions_from_xml_verbatim(system,factor=self.e_factor, model_file=self.model_file, model_name= self.model_name)
        integrate.mode_standard(dt=self.dt)
        rigid = group.rigid()
        nonrigid = group.nonrigid()
        if len(rigid) > 0:
            int_rig = integrate.bdnvt_rigid(group=rigid,T=self.shrinkT, seed=self.seed)
        if len(nonrigid) > 0:
            int_non = integrate.bdnvt(group=nonrigid,T=self.shrinkT, seed=self.seed)
        nlist.reset_exclusions(exclusions=['bond','angle','dihedral','body'])
        N = len(system.particles)
        if N < 50: #Oh god, what's this? TODO
            V = 100.*.524*N/self.phi
        else:
            V = .524*N/self.phi
        L = V**(1/3.)
        print "\n Shrinking to", L, "\n"
        pppm = charge.pppm(group=group.charged())
        pppm.set_params(Nx=64,Ny=64,Nz=64,order=6,rcut=4.0)
        resize=update.box_resize(L = variant.linear_interp([(0,system.box.Lx),(self.shrink_time,L)]))
        #dump.dcd(filename=self.prefix+'shrink.dcd', period=1e4)
        run(self.shrink_time)
        resize.disable()
        run(self.mix_time)
        dump.xml(filename=self.prefix+outfile, **self.xml_args)          
        if len(rigid)>0:
            del int_rig
        if len(nonrigid)>0:
            del int_non
        del system, lj, bonds, angles, dihedrals, rigid, nonrigid, resize, pppm
        init.reset()

    def run(self):
        if self.initialize_new: #read in building blocks, get type info randomize and relax
            self.initialize(factor=self.init_factor)
        if self.randomize_flag:
            self.randomize()
        if self.shrink_flag:
            self.shrink()
        if self.run_type in ['start_from' ,'new']:
            system = init.read_xml(filename=self.infile,time_step=0)
        else:
            system = init.read_xml(filename=self.infile)
        lj, bonds, angles, dihedrals = interactions_from_xml_verbatim(system,factor=self.e_factor, model_file=self.model_file, model_name= self.model_name)
        integrate.mode_standard(dt=self.dt)
        rigid = group.rigid()
        nonrigid = group.nonrigid()
        charged = group.charged()
        if len(charged)>0:
            pppm = charge.pppm(group=charged)
            pppm.set_params(Nx=self.pppm_gridsize,Ny=self.pppm_gridsize,Nz=self.pppm_gridsize, order=self.pppm_gridOrder, rcut=self.pppm_rcut)
        if self.ensemble == 'NVT':
            if len(rigid) > 0:
                int_rig = integrate.nvt_rigid(group=rigid,T=self.T,tau=self.tauT)
            if len(nonrigid) > 0:
                int_non = integrate.nvt(group=nonrigid,T=self.T,tau=self.tauT)
        if self.ensemble == 'NPT':
            if len(rigid) > 0:
                int_rig = integrate.nvt_rigid(group=rigid,T=self.T,tau=self.tauT, tauP=self.tauP)
            if len(nonrigid) > 0:
                int_non = integrate.npt(group=nonrigid,T=self.T,tau=self.tauT, tauP=self.tauP)
        if self.ensemble == 'BDNVT':
            if len(rigid) > 0:
                int_rig = integrate.bdnvt_rigid(group=rigid,T=self.T, seed=self.seed)
            if len(nonrigid) > 0:
                int_non = integrate.bdnvt(group=nonrigid,T=self.T,tau=self.tauT, seed=self.seed)
        nlist.reset_exclusions(exclusions=['bond','angle','dihedral','body'])
        pppm = charge.pppm(group=group.charged())
        pppm.set_params(Nx=64,Ny=64,Nz=64,order=6,rcut=4.0)
        if self.run_type in ['start_from', 'new','from_mav']:
            dump.dcd(filename=self.traj, period=self.dcd_write_time, overwrite=True, unwrap_full=False)
            analyze.msd(filename=self.msd, groups = [group.all()], period=self.dcd_write_time, overwrite = True)
            analyze.log(filename=self.log,quantities=self.log_quants, period=self.log_write_time, header_prefix='#', overwrite=True)
        else:
            dump.dcd(filename=self.traj, period=self.dcd_write_time)
            analyze.msd(filename=self.msd, groups = [group.all()], period=self.dcd_write_time, overwrite = False)
            analyze.log(filename=self.log,quantities=self.log_quants, period=self.log_write_time, header_prefix='#', overwrite=False)
        run_upto(self.tmax, limit_hours=self.stop_hours, limit_multiple=self.log_write_time)
        dump.xml(filename=self.restart,vis=True, image=True, velocity=True)

