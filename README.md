Package for running organic photovoltaic simulations with HOOMD-blue.

- modeler\_hoomd contains the installable python helper functions.
- opv.py holds the opv-specific simulation code
- invoke a simulation with "python inputs/xxx.py"

To install modeler\_hoomd, check out the makefile in modeler\_hoomd and don't forget to add the destination to your PYTHONPATH. 

This code is licensed under the GPLv3 license. For more information please see license.md
