import numpy as np
import cme_utils.builder as bd

#my_bbA = bd.building_block("rbdt-scaled.xml", "model.xml","ua_e")
my_bbA = bd.building_block("rbdt-5-scaled.xml", "model.xml","ua_e")
#my_bbB = bd.building_block("rbdt-scaled.xml", "model.xml","ua_e")
#my_bbA.remove_redundant_rigid_constraints()
#my_bbB.remove_redundant_rigid_constraints()
#my_vec = np.array([0.7,0,0])
#for i in range(4):
#    print("Going to try and connect to: ", i*48+27)
    #my_bbA.write("rbdt-5-scaled.xml")
#my_bbA.join_along_rigid(3*48+27, my_vec, my_bbB, 1, -my_vec)
#my_bbA.join_along_rigid(1*48+27, my_vec, my_bbB, 1, -my_vec)
#my_bbA.remove_redundant_rigid_constraints()
my_bbA.scale(0.42/0.41)
    #my_bbA.write("rbdt-5-scaled.xml")
    #my_bbA = bd.building_block("rbdt-5-scaled.xml", "model.xml","ua_e")
my_bbA.remove_redundant_rigid_constraints()
my_bbA.write("rbdt-5-scaled.xml")
