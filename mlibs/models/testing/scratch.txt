<bond k="30000.0" l="1.5" typeA="C*" typeB="CA" />
<bond k="30000.0" l="1.5" typeA="CA" typeB="CA" />
<bond k="30000.0" l="1.5" typeA="CA" typeB="S" />
<bond k="30000.0" l="1.5" typeA="C*" typeB="S" />
<bond k="30000.0" l="1.5" typeA="CA" typeB="O" />
<bond k="30000.0" l="1.5" typeA="CT" typeB="CT" />
<bond k="30000.0" l="1.5" typeA="CT" typeB="O" />
<bond k="30000.0" l="1.5" typeA="CA" typeB="N" />
<bond k="30000.0" l="1.5" typeA="CT" typeB="N" />

<bond k="19250.0" l="0.411267605634" typeA="C*" typeB="C*" /> no
<bond k="16850.0" l="0.415492957746" typeA="CT" typeB="N" /> yes
<bond k="27300.0" l="0.430704225352" typeA="CA" typeB="CT" /> no
<bond k="12500.0" l="0.481690140845" typeA="CA" typeB="S" /> yes
<bond k="23450.0" l="0.401126760563" typeA="C*" typeB="CA" /> yes
<bond k="27300.0" l="0.388732394366" typeA="CA" typeB="CA" /> yes
<bond k="22500.0" l="0.384225352113" typeA="CA" typeB="O" /> yes
<bond k="21350.0" l="0.389014084507" typeA="CA" typeB="N" /> yes
<bond k="13400.0" l="0.430704225352" typeA="CT" typeB="CT" /> yes
<bond k="12500.0" l="0.481690140845" typeA="C*" typeB="S" /> yes
<bond k="16000.0" l="0.397183098592" typeA="CT" typeB="O" /> ues

<atom atomic_number="0" bond_type="?" charge="0.0" diameter="1.0" epsilon="1.0" id="0" mass="1.0" note="" type="C*" />
<atom atomic_number="0" bond_type="?" charge="0.0" diameter="1.0" epsilon="1.0" id="0" mass="1.0" note="" type="CA" />
<atom atomic_number="0" bond_type="?" charge="0.0" diameter="1.0" epsilon="1.0" id="0" mass="1.0" note="" type="S" />
<atom atomic_number="0" bond_type="?" charge="0.0" diameter="1.0" epsilon="1.0" id="0" mass="1.0" note="" type="O" />
<atom atomic_number="0" bond_type="?" charge="0.0" diameter="1.0" epsilon="1.0" id="0" mass="1.0" note="" type="CT" />
<atom atomic_number="0" bond_type="?" charge="0.0" diameter="1.0" epsilon="1.0" id="0" mass="1.0" note="" type="N" />


<atom atomic_number="6" bond_type="3" charge="0.0" diameter="0.81" epsilon="0.32" id="15" mass="0.37" note="" type="C*" />
<atom atomic_number="6" bond_type="3" charge="0.0" diameter="0.81" epsilon="0.32" id="15" mass="0.41" note="" type="CA" />
<atom atomic_number="8" bond_type="1" charge="0.0" diameter="0.83" epsilon="0.84" id="4" mass="0.5" note="" type="O" />
<atom atomic_number="7" bond_type="3" charge="0.0" diameter="0.92" epsilon="0.68" id="54" mass="0.44" note="" type="N" />
<atom atomic_number="16" bond_type="2" charge="0.0" diameter="1.0" epsilon="1.0" id="26" mass="1.0" note="" type="S" />
<atom atomic_number="6" bond_type="4" charge="0.0" diameter="1.1" epsilon="0.47" id="18" mass="0.44" note="" type="CT" />


<angle k="380.0" theta="1.95" typeA="C*" typeB="CA" typeC="CA" />
<angle k="380.0" theta="1.95" typeA="C*" typeB="S" typeC="CA" />
<angle k="380.0" theta="1.95" typeA="CA" typeB="C*" typeC="S" />
<angle k="380.0" theta="1.95" typeA="CA" typeB="CA" typeC="CA" />
<angle k="380.0" theta="1.95" typeA="CA" typeB="CA" typeC="S" />
<angle k="380.0" theta="1.95" typeA="CA" typeB="CA" typeC="O" />
<angle k="380.0" theta="1.95" typeA="CA" typeB="O" typeC="CT" />
<angle k="380.0" theta="1.95" typeA="CA" typeB="S" typeC="CA" />
<angle k="380.0" theta="1.95" typeA="CA" typeB="CA" typeC="N" />
<angle k="380.0" theta="1.95" typeA="CA" typeB="N" typeC="CA" />
<angle k="380.0" theta="1.95" typeA="CA" typeB="N" typeC="CT" />
<angle k="380.0" theta="1.95" typeA="CT" typeB="CT" typeC="O" />
<angle k="380.0" theta="1.95" typeA="CT" typeB="CT" typeC="CT" />
<angle k="380.0" theta="1.95" typeA="N" typeB="CA" typeC="O" />
<angle k="380.0" theta="1.95" typeA="CT" typeB="CT" typeC="N" />


<angle k="280.549123509" theta="1.96698440556" typeA="CA" typeB="CT" typeC="CA" /> no, does not make sense
<angle k="252.494211158" theta="2.22878357222" typeA="C*" typeB="C*" typeC="CA" /> yes
<angle k="280.549123509" theta="1.96698440556" typeA="CA" typeB="CT" typeC="CT" /> no
<angle k="280.549123509" theta="1.93731383333" typeA="CA" typeB="C*" typeC="CA" /> no
<angle k="233.657341437" theta="1.96698440556" typeA="CT" typeB="CT" typeC="CT" /> yes
<angle k="280.549123509" theta="2.18165972222" typeA="N" typeB="CA" typeC="O" /> yes
<angle k="200.392231078" theta="1.91113391667" typeA="O" typeB="CT" typeC="O" /> no, does not make sense
<angle k="233.657341437" theta="3.14159" typeA="CT" typeB="O" typeC="CT" /> no, does not make sense
<angle k="252.494211158" theta="2.09439333333" typeA="S" typeB="C*" typeC="S" /> no, does not make sense
<angle k="280.549123509" theta="1.96000309444" typeA="C*" typeB="CA" typeC="O" /> no, does not make sense
<angle k="200.392231078" theta="1.91113391667" typeA="CT" typeB="CT" typeC="O" /> yes
<angle k="320.627569725" theta="1.94080448889" typeA="CT" typeB="CT" typeC="N" /> yes
<angle k="252.494211158" theta="2.09439333333" typeA="C*" typeB="C*" typeC="S" /> no, makes sense
<angle k="280.549123509" theta="1.60919221111" typeA="C*" typeB="S" typeC="CA" /> yes
<angle k="280.549123509" theta="1.87273670556" typeA="CA" typeB="CA" typeC="CT" /> no, does not make sense
<angle k="280.549123509" theta="1.9163699" typeA="CA" typeB="N" typeC="CA" /> yes
<angle k="280.549123509" theta="1.60919221111" typeA="CA" typeB="S" typeC="CA" /> yes
<angle k="280.549123509" theta="1.93731383333" typeA="CA" typeB="C*" typeC="S" /> yes
<angle k="280.549123509" theta="1.60919221111" typeA="C*" typeB="S" typeC="C*" /> no, makes sense
<angle k="280.549123509" theta="1.87273670556" typeA="CA" typeB="CA" typeC="CA" /> yes
<angle k="280.549123509" theta="1.9163699" typeA="CA" typeB="N" typeC="CT" /> yes
<angle k="340.666792833" theta="2.3544471722" typeA="C*" typeB="CA" typeC="CA" /> yes
<angle k="280.549123509" theta="1.93731383333" typeA="CA" typeB="CA" typeC="S" /> yes
<angle k="280.549123509" theta="2.16420644444" typeA="CT" typeB="N" typeC="CA" /> yes
<angle k="300.588346617" theta="3.14157313833" typeA="CA" typeB="O" typeC="CT" /> yes
<angle k="280.549123509" theta="2.09439333333" typeA="CA" typeB="CA" typeC="O" /> yes
<angle k="280.549123509" theta="1.87971801667" typeA="CA" typeB="CA" typeC="N" /> yes

