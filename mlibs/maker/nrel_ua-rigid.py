from cme_utils import builder
from cme_utils.builder import model_handler, initialize_system
import numpy

mf = "../models/nrel_ua/model.xml"
mn = "normalized"

def polymerize(): 
    n = "tmp/rbdt1.xml"
    molA = builder.building_block(n,model_name=mn,model_file=mf)
    molB = builder.building_block(n,model_name=mn,model_file=mf)
    molA.join_along_rigid(27,numpy.array([1,0,0]),molB,1,numpy.array([1,0,0]))
    molA.generate_all_constraints()
    molA.write('tmp/rbdt2.xml')
    molA = builder.building_block('tmp/rbdt2.xml',model_name=mn,model_file=mf)
    molB = builder.building_block('tmp/rbdt2.xml',model_name=mn,model_file=mf)
    molA.join_along_rigid(75,numpy.array([1,0,0]),molB,1,numpy.array([1,0,0]))
    molA.write('tmp/rbdt4.xml')
    molA = builder.building_block('tmp/rbdt4.xml',model_name=mn,model_file=mf)
    molB = builder.building_block('tmp/rbdt1.xml',model_name=mn,model_file=mf)
    molA.join_along_rigid(171,numpy.array([1,0,0]),molB,1,numpy.array([1,0,0]))
    molA.scale(0.493417/1.66666)
    molA.write('tmp/rbdt5.xml')
    ####NEED TO SET ID'S AFTER WRITTEN< BEFORE MODEL HANDLER
    #molA = builder.building_block("tmp/bdt5b.xml",model_name=mn,model_file=mf)
    #model_handler.load_opls_into_handler(mf,mn,"oplsaa-ej.prm.txt")
    #molA.set_force_field_specific_parameters()
    #molA.write('tmp/bdt5c.xml')
#polymerize()

initialize_system.make_blob("tmp/rbdt5.xml",mf,mn,T=2.,outfile="blob.xml", calc_charge=False)

