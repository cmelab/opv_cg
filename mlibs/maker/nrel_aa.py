from cme_utils import builder
from cme_utils.builder import model_handler
import numpy

mf = "../models/nrel_aa/model.xml"
mn = "normalized"

n = "../models/nrel_aa/bdt.xml"
molA = builder.building_block(n,model_name=mn,model_file=mf)
molA.generate_all_constraints()
molA.write("tmp/bdt-aa.xml")
molA = builder.building_block('tmp/bdt-aa.xml',model_name=mn,model_file=mf)
molA.set_force_field_specific_parameters()
molA.write('tmp/bdt-aa2.xml')


