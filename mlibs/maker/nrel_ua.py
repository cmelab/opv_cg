from cme_utils import builder
from cme_utils.builder import model_handler, initialize_system
import numpy

mf = "../models/nrel_ua/model.xml"
mn = "normalized"

def prep_cml():
    builder.cml2xml("cml/BDT-TPD-ua.cml","tmp/bdt1.xml",mf,mn)
    builder.cml2xml("cml/pcbm.cml","tmp/pcbm.xml",mf,mn)
#prep_cml()

def polymerize(): 
    n = "tmp/bdt1.xml"
    #molA = builder.building_block(n,model_name=mn,model_file=mf)
    #molB = builder.building_block(n,model_name=mn,model_file=mf)
    #molA.join_along_rigid(27,numpy.array([1,0,0]),molB,1,numpy.array([1,0,0]))
    #molA.generate_all_constraints()
    #molA.write('tmp/bdt2.xml')
    #molA = builder.building_block('tmp/bdt2.xml',model_name=mn,model_file=mf)
    #molB = builder.building_block('tmp/bdt2.xml',model_name=mn,model_file=mf)
    #molA.join_along_rigid(75,numpy.array([1,0,0]),molB,1,numpy.array([1,0,0]))
    #molA.write('tmp/bdt4.xml')
    #molA = builder.building_block('tmp/bdt4.xml',model_name=mn,model_file=mf)
    #molB = builder.building_block('tmp/bdt1.xml',model_name=mn,model_file=mf)
    #molA.join_along_rigid(171,numpy.array([1,0,0]),molB,1,numpy.array([1,0,0]))
    #molA.write('tmp/bdt5.xml')
    #molA = builder.building_block('tmp/bdt5.xml',model_name=mn,model_file=mf)
    #molA.generate_all_constraints()
    #molA.write('tmp/bdt5b.xml')
    ####NEED TO SET ID'S AFTER WRITTEN< BEFORE MODEL HANDLER
    molA = builder.building_block("tmp/bdt_old.xml",model_name=mn,model_file=mf)
    molA.generate_all_constraints()
    #model_handler.load_opls_into_handler(mf,mn,"oplsaa-ej.prm.txt")
    #molA.set_force_field_specific_parameters()
    molA.write('tmp/bdt_old5.xml')
#polymerize()

initialize_system.make_blob("tmp/bdt_old5.xml",mf,mn,T=2.,outfile="blob.xml", calc_charge=False)

