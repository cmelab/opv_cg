from cme_utils import builder
from cme_utils.builder import model_handler
import numpy

mf = "../models/sps/model.xml"
mn = "sps1"

def prep_cml():
    builder.cml2xml("cml/am2.cml","tmp/cation.xml",mf,mn)
    builder.cml2xml("cml/bi2.cml","tmp/anion.xml",mf,mn)
    builder.cml2xml("cml/water.cml","tmp/water.xml",mf,mn)
prep_cml()

def set_params():
    molA = builder.building_block("tmp/cation.xml",model_name=mn, model_file=mf)
    molA.get_parameters_from_model()
    molA.write("tmp/cation2.xml")
    molB = builder.building_block("tmp/anion.xml",model_name=mn, model_file=mf)
    molB.get_parameters_from_model()
    molB.write("tmp/anion2.xml")
    molC = builder.building_block("tmp/water.xml",model_name=mn, model_file=mf)
    molC.get_parameters_from_model()
    molC.write("tmp/water2.xml")
set_params()

